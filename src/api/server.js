import * as axios from 'axios';
import AsyncStorage from '@react-native-community/async-storage';

const server = 'http://202.162.198.182:8082/niaga-pln/public/api';
// const server = 'http://10.16.252.89/niaga-pln/public/api';
// const server = 'http://192.168.1.101/niaga-pln/public/api';

// Check If user is authenticated
let api_token = null;

// (async () => {
//     try{
//         api_token = await AsyncStorage.getItem('api_token')
//     }catch(error){
//         console.log('API:server read api token error')
//     }
// })()

const client = axios.create({
  baseURL: server,
  timeout: 10000,
  headers: {
    'Content-Type': 'application/json',
    Accept: 'application/json',
  },
});

export default client;

class Api {
  constructor() {
    this.api_token = null;
    this.client = null;
    // this.api_url = 'http://10.16.10.70/niaga-pln/public/api';
    this.api_url = server;
  }

  async create() {
    try {
      this.api_token = await AsyncStorage.getItem('api_token');
    } catch (error) {
      console.log('API:server read api token error');
    }

    let headers = {
      'Content-Type': 'application/json',
      Accept: 'application/json',
    };

    if (this.api_token) {
      headers.Authorization = `Bearer ${this.api_token}`;
    }

    this.client = axios.create({
      baseURL: this.api_url,
      timeout: 10000,
      headers: headers,
    });
  }

  getClient() {
    return this.client;
  }

  getUrl() {
    return 'http://202.162.198.182:8082/niaga-pln/public/';
    // return 'http://192.168.1.101/niaga-pln/public/';
  }
}

export {Api};
