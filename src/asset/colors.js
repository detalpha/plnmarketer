const colors = {
  dark: '#3333333',
  grey: '#ECECEC',
  blue: '#0CA3F2',
  darkBlue: '#0075bf',
  border: '#95989A',
  red: '#E62129',
  yellow: '#FED342',
};

export default colors;
