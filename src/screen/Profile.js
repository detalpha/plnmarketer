import React, {Component} from 'react';
import {
  View,
  StyleSheet,
  ImageBackground,
  Image,
  Text,
  TouchableOpacity,
  ToastAndroid,
  AsyncStorage,
} from 'react-native';

// Asset
import colors from '../asset/colors';
import Icon from 'react-native-vector-icons/MaterialIcons';

// API
import server from '../api/server';
import {Api} from '../api/server';

class Profile extends Component {
  constructor(props) {
    super(props);

    // State
    this.state = {
      user: {},
      isDone: false,
    };

    // Bind
    this._onBackPress = this._onBackPress.bind(this);

    this.user = null;
  }

  _showInfo() {
    return (
      <View>
        <Text style={styles.name}>{this.state.user.name}</Text>

        <Text style={styles.textTitle}>Email</Text>
        <Text style={styles.textSubTitle}>{this.state.user.email}</Text>

        <Text style={styles.textTitle}>Level Group</Text>
        <Text style={styles.textSubTitle}>{this.state.user.role.name}</Text>

        <Text style={styles.textTitle}>Unit</Text>
        <Text style={styles.textSubTitle}>
          {this.state.user.unit.nama_area}
        </Text>

        <Text style={styles.textTitle}>Sub Unit</Text>
        <Text style={styles.textSubTitle}>
          {this.state.user.unit.nama_rayon}
        </Text>
      </View>
    );
  }

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.actionbar}>
          <TouchableOpacity onPress={this._onBackPress}>
            <Icon style={styles.icon} name="arrow-back" size={33} />
          </TouchableOpacity>
          <Text style={styles.title}>Profile</Text>
        </View>
        <ImageBackground
          source={require('../asset/Capture2.jpeg')}
          style={styles.imageContainer}>
          <Image
            style={styles.photo}
            source={require('../asset/default_profil.png')}
          />

          {this.state.isDone ? this._showInfo() : <View />}

          {/* <Icon style={ styles.photo } name="check-circle" size={100}/>  */}
        </ImageBackground>
        <View style={styles.profileContainer} />
      </View>
    );
  }

  async componentWillMount() {
    try {
      var session = await AsyncStorage.getItem('user');

      let data = JSON.parse(session);
      console.log('Home:willmount datas', data);

      this.setState({
        user: data,
      });
    } catch (error) {
      console.log('Home:willmount Eror read user data', error);
      throw error;
    }

    this.setState({
      isDone: true,
    });
  }

  componentDidMount() {
    //        // let session =  AsyncStorage.getItem('user')
    //         //let userSession = JSON.parse(session)
    //     console.log('Profile params',this.props.navigation.state.params, session)
  }

  _onBackPress() {
    this.props.navigation.goBack();
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  profileContainer: {
    padding: 12,
    flex: 1,
    marginLeft: 12,
    marginRight: 12,
  },
  name: {
    fontSize: 28,
    fontWeight: 'bold',
    marginTop: 10,
    textAlign: 'center',
    marginBottom: 10,
    color: 'white',
  },
  textTitle: {
    fontSize: 16,
    fontWeight: 'bold',
    marginTop: 5,
    color: 'yellow',
    textAlign: 'center',
  },
  textSubTitle: {
    marginBottom: 5,
    color: '#fff',
    textAlign: 'center',
  },
  separator: {
    height: 0.5,
    marginTop: 8,
    marginBottom: 8,
    backgroundColor: 'grey',
  },

  btnText: {
    color: '#fff',
    fontSize: 16,
    fontWeight: 'bold',
  },
  actionbar: {
    flexDirection: 'row',
    backgroundColor: colors.blue,
    height: 58,
    alignItems: 'center',
  },
  title: {
    flex: 1,
    marginLeft: 12,
    color: '#fff',
    fontWeight: 'bold',
    fontSize: 20,
  },
  icon: {
    color: '#fff',
    marginLeft: 12,
    marginRight: 4,
  },

  imageContainer: {
    justifyContent: 'center',
    alignItems: 'center',
  },

  photo: {
    justifyContent: 'center',
  },
});

export default Profile;
