import React, {Component} from 'react';
import {
  Container,
  Header,
  Left,
  Body,
  Button,
  Icon,
  Title,
  Picker,
  Content,
  Label,
  Form,
  Input,
  Item,
  Text,
} from 'native-base';

import {
  StyleSheet,
  FlatList,
  View,
  ToastAndroid,
  TouchableOpacity,
  ImageBackground,
} from 'react-native';
import ImagePicker from 'react-native-image-picker';
import Geolocation from '@react-native-community/geolocation';
import * as Sentry from '@sentry/react-native';

// Asset
import AddImage from './AddImageButton';

// API
import {Localhost} from '../../api/localhost';

class ProbingForm extends Component {
  constructor(props) {
    super(props);

    // Bind
    this._renderAddButton = this._renderAddButton.bind(this);
    this._getCoordinat = this._getCoordinat.bind(this);
    this._onAddImagePress = this._onAddImagePress.bind(this);
    this._onSavePress = this._onSavePress.bind(this);
    this.onDayaChange = this.onDayaChange.bind(this);
    this.onDayaLamaChange = this.onDayaLamaChange.bind(this);
    this.onTarifChange = this.onTarifChange.bind(this);
    this.onTarifLamaChange = this.onTarifLamaChange.bind(this);
    this.onStatusChange = this.onStatusChange.bind(this);
    this._renderImageList = this._renderImageList.bind(this);
    this._onBackPress = this._onBackPress.bind(this);
    this._getTarifList = this._getTarifList.bind(this);
    this._getDayaList = this._getDayaList.bind(this);
    this._refreshListTarif = this._refreshListTarif.bind(this);
    this._refreshListDaya = this._refreshListDaya.bind(this);

    this.state = {
      form_koordinat: '',
      form_ktp: '',
      form_pelanggan: '',
      form_nohp: '',
      form_merek: '',
      form_kapasitas: '',
      form_qty: '',
      images: [],
      tarif_list: null,
      daya_list: null,
      selectedTarifLama: '-',
      selectedDayaLama: '0',
      selectedTarifBaru: '-',
      selectedDayaBaru: '0',
    };
  }

  _getTarifList() {
    if (this.state.tarif_list) {
      let lists = this.state.tarif_list.map((data, index) => {
        return <Picker.Item key={index} value={data.tipe} label={data.tipe} />;
      });

      return lists;
    }

    return <Picker.Item label="-" value="-" />;
  }

  _getDayaList() {
    if (this.state.daya_list) {
      let lists = this.state.daya_list.map((data, index) => {
        return <Picker.Item key={index} value={data.daya} label={data.daya} />;
      });

      return lists;
    }

    return <Picker.Item label="-" value="-" />;
  }

  render() {
    const jenis = this.props.navigation.state.params.type;

    return (
      <Container>
        <Header>
          <Left>
            <Button transparent onPress={() => this._onBackPress(1)}>
              <Icon name="arrow-back" />
            </Button>
          </Left>
          <Body>
            <Title>Probing</Title>
          </Body>
        </Header>
        <Content>
          <Form>
            <Item stackedLabel>
              <Label>Nama Pelanggan</Label>
              <Input
                onChangeText={text =>
                  this.setState({form_pelanggan: text}, () => {
                    console.log('form_pelanggan', this.state.form_pelanggan);
                  })
                }
              />
            </Item>
            <Item stackedLabel>
              <Label>No KTP</Label>
              <Input onChangeText={text => this.setState({form_ktp: text})} />
            </Item>
            <Item stackedLabel>
              <Label>No Agenda</Label>
              <Input
                onChangeText={text => this.setState({form_no_agenda: text})}
              />
            </Item>
            <Item stackedLabel>
              <Label>Alamat</Label>
              <Input
                onChangeText={text => this.setState({form_alamat: text})}
              />
            </Item>
            <Item stackedLabel>
              <Label>No Hand Phone</Label>
              <Input onChangeText={text => this.setState({form_nohp: text})} />
            </Item>
            {jenis == 'PB' ? (
              <View>
                <Item picker>
                  <Label>Tarif</Label>
                  <Picker
                    mode="dropdown"
                    iosIcon={<Icon name="arrow-down" />}
                    placeholder="Pilih Tarif"
                    placeholderStyle={{color: '#bfc6ea'}}
                    placeholderIconColor="#007aff"
                    selectedValue={this.state.selectedTarifBaru}
                    onValueChange={this.onTarifChange}>
                    <Picker.Item label="-" value="-" />
                    {this._getTarifList()}
                  </Picker>
                </Item>
                <Item picker>
                  <Label>Daya</Label>
                  <Picker
                    mode="dropdown"
                    iosIcon={<Icon name="arrow-down" />}
                    placeholder="Pilih Daya"
                    placeholderStyle={{color: '#bfc6ea'}}
                    placeholderIconColor="#007aff"
                    selectedValue={this.state.selectedDayaBaru}
                    onValueChange={this.onDayaChange}>
                    <Picker.Item label="-" value="0" />
                    {this._getDayaList()}
                  </Picker>
                </Item>
              </View>
            ) : null}
            {jenis == 'PD' ? (
              <View>
                <Item picker>
                  <Label>Tarif Lama</Label>
                  <Picker
                    mode="dropdown"
                    iosIcon={<Icon name="arrow-down" />}
                    placeholder="Pilih Tarif Lama"
                    placeholderStyle={{color: '#bfc6ea'}}
                    placeholderIconColor="#007aff"
                    selectedValue={this.state.selectedTarifLama}
                    onValueChange={this.onTarifChange}>
                    <Picker.Item label="-" value="-" />
                    {this._getTarifList()}
                  </Picker>
                </Item>
                <Item picker>
                  <Label>Daya Lama</Label>
                  <Picker
                    mode="dropdown"
                    iosIcon={<Icon name="arrow-down" />}
                    placeholder="Pilih Daya Lama"
                    placeholderStyle={{color: '#bfc6ea'}}
                    placeholderIconColor="#007aff"
                    selectedValue={this.state.selectedDayaLama}
                    onValueChange={this.onDayaLamaChange}>
                    <Picker.Item label="-" value="0" />
                    {this._getDayaList()}
                  </Picker>
                </Item>
                <Item picker>
                  <Label>Tarif Baru</Label>
                  <Picker
                    mode="dropdown"
                    iosIcon={<Icon name="arrow-down" />}
                    placeholder="Pilih Tarif Baru"
                    placeholderStyle={{color: '#bfc6ea'}}
                    placeholderIconColor="#007aff"
                    selectedValue={this.state.selectedTarifBaru}
                    onValueChange={this.onTarifChange}>
                    <Picker.Item label="-" value="-" />
                    {this._getTarifList()}
                  </Picker>
                </Item>
                <Item picker>
                  <Label>Daya Baru</Label>
                  <Picker
                    mode="dropdown"
                    iosIcon={<Icon name="arrow-down" />}
                    placeholder="Pilih Daya Baru"
                    placeholderStyle={{color: '#bfc6ea'}}
                    placeholderIconColor="#007aff"
                    selectedValue={this.state.selectedDayaBaru}
                    onValueChange={this.onDayaChange}>
                    <Picker.Item label="-" value="0" />
                    {this._getDayaList()}
                  </Picker>
                </Item>
              </View>
            ) : null}
            <Item stackedLabel>
              <Label>Tagging Koordinat</Label>
              <Input value={this.state.form_koordinat} editable={false} />
            </Item>
            <View style={styles.imageList}>
              <FlatList
                horizontal={true}
                data={this.state.images}
                keyExtractor={this._keyExtractor}
                ListFooterComponent={this._renderAddButton()}
                renderItem={this._renderImageList}
              />
            </View>
          </Form>
          <Button block info onPress={this._onSavePress} style={styles.button}>
            <Text>SUBMIT</Text>
          </Button>
        </Content>
      </Container>
    );
  }

  componentDidMount() {
    this._getCoordinat();
    this._refreshListTarif();
    this._refreshListDaya();
  }

  onDayaChange(value) {
    this.setState({
      selectedDayaBaru: value,
    });
  }

  onDayaLamaChange(value) {
    this.setState({
      selectedDayaLama: value,
    });
  }

  onTarifChange(value) {
    this.setState({
      selectedTarifBaru: value,
    });
  }

  onTarifLamaChange(value) {
    this.setState({
      selectedTarifLama: value,
    });
  }

  onStatusChange(value) {
    this.setState({
      selectedStatus: value,
    });
  }

  _renderAddButton() {
    // Max Limit 10 images
    if (this.state.images.length >= 10) {
      return;
    }

    // Hide button when still upload image
    let uploadingImages = this.state.images.filter(image => {
      return image.isUploading;
    });

    if (uploadingImages.length > 0) {
      // return
    }

    return (
      <AddImage
        style={{width: 135, height: 135, margin: 2.5}}
        onPress={this._onAddImagePress}
      />
    );
  }

  _renderImageList({item, index}) {
    let maskText;
    let styleMask;

    if (item.isUploading) {
      maskText = <Text style={{color: 'red'}}>Uploading</Text>;
    }

    if (item.isUploadError) {
      maskText = <Text style={{color: 'red'}}>Error</Text>;
    }

    if (item.isUploadError || item.isUploading) {
      styleMask = {backgroundColor: 'rgba(0,0,0,0.6)'};
    }

    return (
      <TouchableOpacity
        style={[
          {
            width: 135,
            height: 135,
            margin: 2.5,
            backgroundColor: 'lightgrey',
            elevation: 3,
          },
        ]}
        onPress={() => this._renderPreview(item, index)}>
        <ImageBackground
          source={{uri: item.url}}
          style={[
            {
              width: 135,
              height: 135,
              justifyContent: 'center',
              alignItems: 'center',
            },
            styleMask,
          ]}>
          {maskText}
          <View
            style={{
              bottom: 0,
              left: 0,
              position: 'absolute',
              marginTop: 20,
              width: 100,
              backgroundColor: 'rgba(0,0,0,0.3)',
            }}
          />
        </ImageBackground>
      </TouchableOpacity>
    );
  }

  _onAddImagePress() {
    console.log('Add Image Press');

    let options = {
      title: 'Select Image',
      noData: true,
      storageOptions: {
        cameraRoll: true,
      },
      //penambahan location

      maxWidth: 8000,
      maxHeight: 8000,
      path: 'images',
    };

    ImagePicker.launchCamera(options, response => {
      console.log('Image Picker Response', response);

      // Cancel
      if (response.didCancel) {
        console.log('Canceled');
        return;
      }

      // Error
      if (response.error) {
        console.log('Error', response.error);
        Sentry.captureException(response.error);
        return;
      }

      if (response.uri) {
        let images = this.state.images;

        console.log('ini', response, options);

        images.push({
          url: response.uri,
          type: response.type,
          name: response.fileName,
          isLocal: true,
          isUploading: false,
          isUploadError: false,
        });

        this.setState(
          {
            images: images,
          },
          () => {
            console.log('image state after add from picker', this.state.images);
          },
        );
      }
    });
  }

  _getCoordinat() {
    //Get location
    if (this.props.screenProps.isConnected) {
      Geolocation.getCurrentPosition(
        position => {
          console.log(position);
          this.setState({
            // isLoading:false,
            form_koordinat:
              position.coords.latitude + ',' + position.coords.longitude,
            latitude: position.coords.latitude,
            longitude: position.coords.longitude,
            koordinat:
              position.coords.latitude + ',' + position.coords.longitude,
          });
        },
        error => Sentry.captureException(error),
        {enableHighAccuracy: false, timeout: 20000, maximumAge: 10000},
      );
    } else {
      Geolocation.getCurrentPosition(
        position => {
          console.log(position);
          this.setState({
            // isLoading:false,
            form_koordinat:
              position.coords.latitude + ',' + position.coords.longitude,
            latitude: position.coords.latitude,
            longitude: position.coords.longitude,
            koordinat:
              position.coords.latitude + ',' + position.coords.longitude,
          });
        },
        error => Sentry.captureException(error),
        {enableHighAccuracy: false, timeout: 20000, maximumAge: 10000},
      );
    }
  }

  async _onSavePress() {
    console.log('mulai save', this.state);
    // New Localhost
    let api = new Localhost();
    // Create API
    await api.create();

    let client = api.getClient();

    let koordinat = this.state.form_koordinat.split(',');

    // Post data to server
    let data = {
      no_ktp: this.state.form_ktp,
      type: this.props.navigation.state.params.type,
      nama_pelanggan: this.state.form_pelanggan,
      tarif_baru: this.state.selectedTarifBaru,
      daya_baru: this.state.selectedDayaBaru,
      tarif_lama: this.state.selectedTarifLama,
      daya_lama: this.state.selectedDayaLama,
      koordinat_x: koordinat[0],
      koordinat_y: koordinat[1],
      is_probing_individual: true,
      merek: this.state.form_merek,
      kapasitas: this.state.form_kapasitas,
      qty_unit: this.state.form_qty,
      status: this.state.selectedStatus,
      no_agenda: this.state.form_no_agenda,
      alamat: this.state.form_alamat,
    };

    console.log('Request data', data);

    this.setState({
      btn_loading: true,
      btn_disabled: true,
    });

    await client
      .post('/probing', data)
      .then(async response => {
        console.log('Post Laporan WO Response', response.data);

        if (response.status === 200) {
          this.setState({
            id_probing: response.data.id,
          });

          await this._uploadImage();
          this.props.navigation.navigate('Main');
        } else {
          ToastAndroid.show(
            'Failed create WO, please check your data',
            ToastAndroid.SHORT,
          );

          this.setState({
            btn_loading: false,
            btn_disabled: false,
          });
        }
      })
      .catch(error => {
        console.log('Error create post data', error);
        console.log('Error', error.response);
        console.log('Request', error.request);
        Sentry.captureException(error);
        ToastAndroid.show('Error send post data to server', ToastAndroid.SHORT);

        this.setState({
          btn_loading: false,
          btn_disabled: false,
        });
      });
  }

  async _uploadImage() {
    let api = new Localhost();
    // Create API
    await api.create();

    let client = api.getClient();
    let config = {
      onUploadProgress: function(progressEvent) {
        var percentCompleted = Math.round(
          (progressEvent.loaded * 100) / progressEvent.total,
        );
        console.log('Uploading: ' + percentCompleted + '%');
      },
    };

    client.defaults.headers['Content-Type'] = 'multipart/form-data';

    // Add Data
    let images = this.state.images;

    let localImages = images
      .map((image, index) => {
        image.index = index;
        return image;
      })
      .filter((image, index) => {
        return image.isLocal;
      });

    localImages.forEach(image => {
      // Upload to server
      let form = new FormData();
      form.append('description', this.state.form_komponen);
      form.append('image', {
        uri: image.url,
        type: image.type,
        name: image.name,
      });

      // Update uploading state
      image.isUploading = true;
      image.isUploadError = false;
      images[image.index] = image;

      this.setState({
        images: images,
      });

      // REST Api
      client
        .post('/probing/images/' + this.state.id_probing, form, config)
        .then(response => {
          console.log('Upload image ', response);

          if (response.status === 200) {
            console.log('Upload image success');

            // Update Success state
            let index = image.index;
            image = response.data;

            image.index = index;
            image.isUploading = false;
            image.isLocal = false;

            // Update Peview
            this.setState({
              selectedImage: image.url,
              selectedImageIndex: index,
            });

            this.setState({
              form_jenis: 'JTM',
              form_tiang: null,
              form_gardu: null,
              form_catatan: null,
              form_prioritas: 'Buruk',
              form_koordinat: null,
              selected_rayon: '',
              id_rayon_terpilih: '',
              images: [],
            });
          } else {
            console.log('Upload image error');
            image.isUploading = false;
            image.isLocal = false;
            image.isUploadError = true;

            // if (fromSavePress) {
            //   this._destroyCkr();
            // }
          }
        })
        .catch(error => {
          console.log('Upload Error ', error);
          Sentry.captureException(error);

          image.isUploading = false;
          image.isUploadError = true;
          image.isLocal = true;

          // if (fromSavePress) {
          //   this._destroyCkr();
          // }
        });
    });
  }

  _onBackPress() {
    this.props.navigation.goBack();
  }

  async _refreshListTarif() {
    // New API
    let api = new Localhost();
    // Create API
    await api.create();

    let client = api.getClient();

    this.setState({
      btn_loading: true,
    });

    client
      .get('/pickerTarif')
      .then(response => {
        if (response.status == 200) {
          this.setState({
            tarif_list: response.data,
            btn_loading: false,
          });

          this._getTarifList();
        }
      })
      .catch(error => {
        console.log('Error Fetch Data', error.response);
        Sentry.captureException(error);
        ToastAndroid.show('Error fetch Penyulang Data', ToastAndroid.SHORT);

        this.setState({
          btn_loading: false,
        });
      });
  }

  async _refreshListDaya() {
    // New API
    let api = new Localhost();
    // Create API
    await api.create();

    let client = api.getClient();

    this.setState({
      btn_loading: true,
    });

    client
      .get('/pickerDaya')
      .then(response => {
        if (response.status == 200) {
          console.log(response);
          this.setState({
            daya_list: response.data,
            btn_loading: false,
          });

          this._getDayaList();
        }
      })
      .catch(error => {
        console.log('Error Fetch Data', error.response);
        Sentry.captureException(error);
        ToastAndroid.show('Error fetch Penyulang Data', ToastAndroid.SHORT);

        this.setState({
          btn_loading: false,
        });
      });
  }
}

const styles = StyleSheet.create({
  button: {
    margin: 5,
  },
  imageList: {
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 20,
  },
});

export default ProbingForm;
