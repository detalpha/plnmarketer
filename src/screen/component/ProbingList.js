import React, {Component} from 'react';
import {
  Container,
  Header,
  Left,
  Body,
  Button,
  Icon,
  Title,
  Content,
} from 'native-base';

import {FlatList, StyleSheet} from 'react-native';
import {ListItem} from 'react-native-elements';
import * as Sentry from '@sentry/react-native';

// Asset
import colors from '../../asset/colors';

// API
import {Api} from '../../api/server';

class PdlList extends Component {
  constructor(props) {
    super(props);

    this.state = {
      data: [],
    };

    this._onBackPress = this._onBackPress.bind(this);
    this._refreshData = this._refreshData.bind(this);
  }

  _onBackPress() {
    this.props.navigation.goBack();
  }

  async componentDidMount() {
    this._refreshData();
  }

  async _refreshData() {
    // console.log('Load Data WO')
    // New API
    let api = new Api();
    // Create API
    await api.create();

    let client = api.getClient();

    client
      .get('/list/probing')
      .then(response => {
        console.log(response.data);
        if (response.status == 200) {
          this.setState({
            data: response.data,
            isLoading: false,
          });
        }
      })
      .catch(error => {
        console.log('RefreshWo:error', error);
        Sentry.captureException(error);
      });
  }

  render() {
    return (
      <Container>
        <Header>
          <Left>
            <Button transparent onPress={() => this._onBackPress(1)}>
              <Icon name="arrow-back" />
            </Button>
          </Left>
          <Body>
            <Title>Pending Probing</Title>
          </Body>
        </Header>
        <Content>
          <FlatList
            data={this.state.data}
            renderItem={({item}) => (
              <ListItem
                button
                onPress={() =>
                  this.props.navigation.navigate('ProbingDetail', {
                    id: item.id,
                    isPending: 1,
                  })
                }
                roundAvatar
                title={`Nama Pelanggan: ${item.nama_pelanggan}`}
                subtitle={item.type}
                containerStyle={{borderBottomWidth: 1}}
              />
            )}
          />
        </Content>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  actionbar: {
    flexDirection: 'row',
    backgroundColor: colors.blue,
    height: 58,
    alignItems: 'center',
  },
  icon: {
    color: '#fff',
    marginLeft: 12,
    marginRight: 4,
  },
  title: {
    flex: 1,
    marginLeft: 12,
    color: '#fff',
    fontSize: 20,
    fontWeight: 'bold',
  },
  viewTouchable: {
    flex: 1,
    backgroundColor: 'white',
    alignItems: 'center',
    paddingVertical: 20,
    elevation: 5,
  },
  imageTouchable: {
    width: 75,
    height: 75,
    justifyContent: 'center',
    alignItems: 'center',
  },
  separator: {
    height: 0.5,
    marginTop: 8,
    marginBottom: 8,
    backgroundColor: 'grey',
  },
  buttonRow: {
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
    marginTop: 10,
  },
  ModalInsideView: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#fff',
    height: '60%',
    width: '90%',
    borderRadius: 10,
    borderWidth: 3,
    borderColor: '#ccc',
  },
  ModalFormInsideView: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#fff',
    width: '100%',
  },
  pickerForm: {
    width: '60%',
    backgroundColor: colors.grey,
    height: 45,
    padding: 12,
  },
  textAreaForm: {
    width: '60%',
    backgroundColor: colors.grey,
    padding: 12,
    marginBottom: 8,
  },
});

export default PdlList;
