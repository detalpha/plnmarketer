import React, {Component} from 'react';
import {
  Container,
  Header,
  Left,
  Body,
  Button,
  Icon,
  Title,
  Content,
  Text,
} from 'native-base';

import {Dimensions, StyleSheet} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';

// Asset
import colors from '../../asset/colors';

// API
import server from '../../api/server';
import {Api} from '../../api/server';

// Navigation
import {NavigationActions, StackActions} from 'react-navigation';

class ProbingHome extends Component {
  constructor(props) {
    super(props);

    // Bind
    this._showProbingFrom = this._showProbingFrom.bind(this);
    this._onBackPress = this._onBackPress.bind(this);

    this.state = {};
  }

  render() {
    return (
      <Container>
        <Header>
          <Left>
            <Button transparent onPress={() => this._onBackPress(1)}>
              <Icon name="arrow-back" />
            </Button>
          </Left>
          <Body>
            <Title>Pilih Probing</Title>
          </Body>
        </Header>
        <Content>
          <Button
            block
            info
            onPress={() => this._showProbingFrom(1)}
            style={styles.button}>
            <Text>Non Kolektif</Text>
          </Button>
          <Button
            block
            info
            onPress={() => this._showProbingFrom(2)}
            style={styles.button}>
            <Text>Kolektif</Text>
          </Button>
        </Content>
      </Container>
    );
  }

  _showProbingFrom(isKolektif) {
    this.props.navigation.navigate('ProbingForm', {
      type: this.props.navigation.state.params.type,
      isKolektif: isKolektif === 1 ? true : false,
    });
  }

  _onBackPress() {
    this.props.navigation.goBack();
  }
}

const styles = StyleSheet.create({
  button: {
    margin: 5,
  },
});

export default ProbingHome;
