import React, {Component} from 'react';
import {Button, Text, View, Card, CardItem, Body} from 'native-base';

import {Dimensions, StyleSheet} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';

// Asset
import colors from '../../asset/colors';

// API
import server from '../../api/server';
import {Api} from '../../api/server';

// Navigation
import {NavigationActions, StackActions} from 'react-navigation';

class Main extends Component {
  constructor(props) {
    super(props);

    this.state = {
      drawerOpen: false,
      user: {},
      isLoading: false,
      BadgeIcon: 0,
      currentBadge: 0,
      currentBadgeLaporan: 0,
      currentBadgeHar: 0,
      currentBadgeHarLaporan: 0,
      currentBadgeLaporanOffline: 0,
    };
  }

  render() {
    return (
      <View>
        <Card>
          <CardItem>
            <Body>
              <Button
                block
                info
                onPress={() => {
                  this.setState({currentView: 'market', tipeAksi: 'pb'});
                }}>
                <Text>Pasang Baru</Text>
              </Button>
              <Button
                block
                info
                onPress={() => {
                  this.setState({currentView: 'market', tipeAksi: 'pd'});
                }}>
                <Text>Perubahan Daya</Text>
              </Button>
            </Body>
          </CardItem>
        </Card>
      </View>
    );
  }
}

const win = Dimensions.get('window');

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'gainsboro',
  },
  slide: {
    backgroundColor: '#9DD6EB',
    height: 140,
    alignItems: 'center',
    justifyContent: 'center',
  },
  body: {
    backgroundColor: 'grey',
  },
  imageslide: {
    flex: 1,
    width: win.width,
    height: win.height,
  },
  actionbar: {
    flexDirection: 'row',
    backgroundColor: colors.blue,
    height: 58,
    alignItems: 'center',
    elevation: 1,
  },
  icon: {
    color: '#fff',
    marginLeft: 12,
    marginRight: 4,
  },
  title: {
    marginLeft: 12,
    color: '#fff',
    fontWeight: 'bold',
    fontSize: 20,
    flex: 1,
  },
  namauser: {
    marginRight: 12,
    textAlign: 'right',
    color: '#fff',
    fontWeight: 'bold',
    fontSize: 15,
    flex: 1,
  },
});

const drawerStyles = {
  drawer: {
    shadowColor: '#000000',
    shadowOpacity: 0.8,
    shadowRadius: 3,
    elevation: 1,
  },
};

export default Main;
