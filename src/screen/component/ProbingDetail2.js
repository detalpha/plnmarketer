import React, {Component} from 'react';
import {
  Container,
  Header,
  Left,
  Body,
  Button,
  Icon,
  Title,
  Content,
  Text,
} from 'native-base';

import {Dimensions, StyleSheet, Image} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';

// Asset
import colors from '../../asset/colors';

// API
import server from '../../api/server';
import {Localhost} from '../../api/localhost';

// Navigation
import {NavigationActions, StackActions} from 'react-navigation';

class ProbingDetail extends Component {
  constructor(props) {
    super(props);
    this._refreshData = this._refreshData.bind(this);

    this.state = {
      data: {},
    };
  }

  render() {
    return (
      <Container>
        <Header>
          <Left>
            <Button transparent>
              <Icon name="arrow-back" />
            </Button>
          </Left>
          <Body>
            <Title>Probing Detail</Title>
          </Body>
        </Header>
        <Content padder>
          <Image
            style={{width: Dimensions.get('window').width / 1.05, height: 400}}
            source={{
              uri:
                'http://202.162.198.182:8082/niaga-pln/public/' +
                this.state.data.path_foto,
            }}
          />
          <Text>{this.state.data.nama_pelanggan}</Text>
        </Content>
      </Container>
    );
  }

  async componentDidMount() {
    await this._refreshData();
  }

  async _refreshData() {
    let probingId = this.props.navigation.getParam('id');
    console.log('refresh data');
    // New API
    let api = new Localhost();
    await api.create();

    let client = api.getClient();
    // Set Loading
    this.setState({isLoading: true});

    // client.get('/laporanCkr/temuan').then((response) => {
    await client
      .get('/probing/' + probingId)
      .then(response => {
        console.log('refresh data', response.data);

        if (response.status === 200) {
          this.setState({
            data: response.data,
            isLoading: false,
          });
        }
      })
      .catch(error => {
        // console.log('JobList::error ',error)
        // Set Loading
        this.setState({isLoading: false});
      });
  }
}

const styles = StyleSheet.create({
  button: {
    margin: 5,
  },
});

export default ProbingDetail;
