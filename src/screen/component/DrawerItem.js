import React, {Component} from 'react';
import {View, TouchableOpacity, Text, StyleSheet} from 'react-native';

// Asset
import colors from '../../asset/colors';
import Icon from 'react-native-vector-icons/MaterialIcons';

import TextCustom from './MyText';

class DrawerItem extends Component {
  render() {
    return (
      <TouchableOpacity onPress={this.props.onPress || null}>
        <View style={styles.content}>
          <Icon
            style={styles.icon}
            name={this.props.icon || 'account-circle'}
            size={33}
          />
          <TextCustom style={styles.text}>
            {this.props.menu || 'Menu'}
          </TextCustom>
        </View>
      </TouchableOpacity>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  content: {
    flexDirection: 'row',
    padding: 8,
    marginLeft: 12,
    marginRight: 12,
    marginTop: 8,
    alignItems: 'center',
  },
  text: {
    fontSize: 18,
    marginLeft: 12,
  },
  icon: {
    color: colors.blue,
  },
});

export default DrawerItem;
