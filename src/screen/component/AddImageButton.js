import React from 'react';
import {View, TouchableOpacity, Text, StyleSheet} from 'react-native';

// Asset
import Icon from 'react-native-vector-icons/MaterialIcons';

class AddImageButton extends React.Component {
  render() {
    return (
      <TouchableOpacity onPress={this.props.onPress}>
        <View style={[styles.container, this.props.style]}>
          <Icon name="camera-enhance" size={50} />
          <Text>Upload Foto</Text>
        </View>
      </TouchableOpacity>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    backgroundColor: 'silver',
    justifyContent: 'center',
    alignItems: 'center',
  },
});
export default AddImageButton;
