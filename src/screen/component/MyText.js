import React from 'react';
import {Text as TextCustom, StyleSheet} from 'react-native';

class MyText extends React.Component {
  render() {
    return (
      <TextCustom style={[styles.container, this.props.style]}>
        {this.props.children}
      </TextCustom>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    fontFamily: 'sans-serif-light',
    fontSize: 11,
  },
});
export default MyText;
