import React, {Component} from 'react';
import {Button, Text, View, Card, CardItem, Body} from 'native-base';

import {Dimensions, StyleSheet} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';

// Asset
import colors from '../../asset/colors';

// API
import server from '../../api/server';
import {Api} from '../../api/server';

// Navigation
import {NavigationActions, StackActions} from 'react-navigation';

class PdlHome extends Component {
  constructor(props) {
    super(props);

    // Bind
    this._refreshData = this._refreshData.bind(this);

    this.state = {
      drawerOpen: false,
      user: {},
      isLoading: false,
      BadgeIcon: 0,
      currentBadge: 0,
      currentBadgeLaporan: 0,
      currentBadgeHar: 0,
      currentBadgeHarLaporan: 0,
      currentBadgeLaporanOffline: 0,
    };
  }

  render() {
    return (
      <View>
        <Card>
          <CardItem>
            <Body>
              <Button
                block
                info
                onPress={() => {
                  this.setState({currentView: 'market', tipeAksi: 'pb'});
                }}>
                <Text>Pasang Baru</Text>
              </Button>
              <Button
                block
                info
                onPress={() => {
                  this.setState({currentView: 'market', tipeAksi: 'pd'});
                }}>
                <Text>Perubahan Daya</Text>
              </Button>
              <Button
                block
                info
                onPress={() => {
                  this.setState({currentView: 'market', tipeAksi: 'ac'});
                }}>
                <Text>Akusisi Captive</Text>
              </Button>
            </Body>
          </CardItem>
        </Card>
      </View>
    );
  }
  async _refreshData() {
    console.log('run');
    let user = JSON.parse(await AsyncStorage.getItem('user'));
    console.log('user data', user);
    let offlineList = JSON.parse(
      await AsyncStorage.getItem(user.id + 'Laporan'),
    );
    let offlinecount = offlineList.list.length;
    this.setState(
      {
        user: user,
        name_user: user.name,
        currentBadgeLaporanOffline: offlinecount,
      },
      () => {
        console.log('state', this.state);
      },
    );
  }

  async _componentDidMount() {
    console.log('masuk pak eko');
    await this._refreshData();
  }

  async componentWillMount() {
    try {
      let session = await AsyncStorage.getItem('user');
      let data = JSON.parse(session);
      console.log('Home:willmount data', data);

      this.setState({
        user: data,
        name_user: data.name,
      });
    } catch (error) {
      console.log('Home:willmount Eror read user data', error);
    }
  }

  _openForm(tipe) {
    if (tipe === 'probing') {
      this.props.navigation.navigate('ProbingForm', {
        tipeAksi: this.state.tipeAksi,
      });
    } else {
      this.props.navigation.navigate('PdlForm', {
        tipeAksi: this.state.tipeAksi,
      });
    }
  }
}

const win = Dimensions.get('window');

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'gainsboro',
  },
  slide: {
    backgroundColor: '#9DD6EB',
    height: 140,
    alignItems: 'center',
    justifyContent: 'center',
  },
  body: {
    backgroundColor: 'grey',
  },
  imageslide: {
    flex: 1,
    width: win.width,
    height: win.height,
  },
  actionbar: {
    flexDirection: 'row',
    backgroundColor: colors.blue,
    height: 58,
    alignItems: 'center',
    elevation: 1,
  },
  icon: {
    color: '#fff',
    marginLeft: 12,
    marginRight: 4,
  },
  title: {
    marginLeft: 12,
    color: '#fff',
    fontWeight: 'bold',
    fontSize: 20,
    flex: 1,
  },
  namauser: {
    marginRight: 12,
    textAlign: 'right',
    color: '#fff',
    fontWeight: 'bold',
    fontSize: 15,
    flex: 1,
  },
});

const drawerStyles = {
  drawer: {
    shadowColor: '#000000',
    shadowOpacity: 0.8,
    shadowRadius: 3,
    elevation: 1,
  },
};

export default PdlHome;
