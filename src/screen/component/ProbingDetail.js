import React from 'react';
import {
  View,
  StyleSheet,
  TouchableOpacity,
  Text,
  ScrollView,
  Modal,
  ToastAndroid,
} from 'react-native';
import Slideshow from 'react-native-image-slider-show';
import ImageViewer from 'react-native-image-zoom-viewer';
import {NavigationActions, StackActions} from 'react-navigation';
import * as Sentry from '@sentry/react-native';

// Component
import {Card, Button} from 'react-native-elements';
import colors from '../../asset/colors';
import TextPegawai from './MyText';
import Icons from 'react-native-vector-icons/MaterialIcons';

// API
import {Localhost} from '../../api/localhost';

class ProbingDetail extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      no_ktp: null,
      form_gardu: null,
      form_catatan: null,
      form_vendor: null,
      dialogPilihAssetVisible: false,
      dialogCatatanVisible: false,
      ModalImageVisible: false,
      ModalRayonVisibleStatus: false,
      ModalAcceptVisibleStatus: false,
      ModalRejectVisibleStatus: false,
      ModalVendorVisibleStatus: false,
      imageUrls: [],
      currentlat: null,
      currentlong: null,

      id_laporan_inspeksi: null,
      images: [],
      catatan: null,
      catatan_rayon: null,
      tanggal_inspeksi: null,
      id_rayon: null,
      nama_rayon: null,
      lokasi_pgw: null,
      inspektor: null,
      koordinat_pgw: '',
      progres_status: null,

      selected_penyulang: null,
      selected_penyulang_text: '',
      pickerPenyulangVisible: false,
      pickerPenyulangOption: [],
      penyulang_list: [],

      pickerRayonVisible: false,
      pickerRayonOption: [],
      selected_rayon: '',
      selected_rayon_text: '',
      rayon_list: [],

      pickerVendorVisible: false,
      pickerVendorOption: [],
      selected_vendor: '',
      selected_vendor_text: '',
      vendor_list: [],
      isPending: 0,
    };

    this._onBackPress = this._onBackPress.bind(this);
    this._refreshData = this._refreshData.bind(this);
    this._onSubmitClosing = this._onSubmitClosing.bind(this);
  }

  showModalImageFunction(visible, data) {
    const imageUrl = [];

    if (data) {
      imageUrl.push({url: data.image.url});
    }

    this.setState({ModalImageVisible: visible, imageUrls: imageUrl});
  }

  ShowModalPDLFunction(visible) {
    this.setState({ModalRayonVisibleStatus: visible});
  }

  _onBackPress() {
    this.props.navigation.goBack();
  }

  async componentDidMount() {
    await this._refreshData();
    let isPending = this.props.navigation.getParam('isPending');

    this.setState({isPending});
  }

  async _refreshData() {
    // console.log('Load Data WO')
    let probingId = this.props.navigation.getParam('id');
    // New API
    let api = new Localhost();
    // Create API
    await api.create();

    let client = api.getClient();
    let api_url = api.getUrl();

    // Set Loading
    this.setState({isLoading: true});

    client
      .get('/probing/' + probingId)
      .then(response => {
        console.log(response.data);
        if (response.status == 200) {
          const {
            no_ktp,
            nama_pelanggan,
            tarif,
            daya,
            data_pembangkit,
            koordinat_x,
            koordinat_y,
            is_probing_individual,
            path_foto,
            path_ktp,
            created_by,
          } = response.data;

          let {photo} = response.data;

          if (!photo) {
            photo = [
              {
                url: require('../../asset/no-image-available.jpg'),
              },
            ];
          } else {
            if (photo.length > 0) {
              photo.map(function(photos) {
                photos.url = api_url + photos.path_foto;

                return photos;
              });
            }
          }

          this.setState({
            probingId,
            no_ktp,
            nama_pelanggan,
            tarif,
            daya,
            data_pembangkit,
            koordinat_x,
            koordinat_y,
            is_probing_individual,
            path_foto,
            path_ktp,
            created_by,
            images: photo,
            isLoading: false,
          });
        }
      })
      .catch(error => {
        console.log('RefreshWo:error', error);
        Sentry.captureException(error);
      });
  }

  async _onSubmitClosing() {
    // New API
    let api = new Localhost();
    // Create API
    await api.create();

    let client = api.getClient();

    // Post data to server
    let data = {
      id_pelanggan: this.state.probingId,
    };

    console.log(data);

    await client
      .post('/deal', data)
      .then(async response => {
        if (response.status == 200) {
          ToastAndroid.show('Data berhasil di closing', ToastAndroid.SHORT);

          this.ShowModalPDLFunction(false);
          const resetAction = StackActions.reset({
            index: 0,
            actions: [NavigationActions.navigate({routeName: 'Main'})],
          });
          this.props.navigation.dispatch(resetAction);
        } else {
          ToastAndroid.show('Closing gagal', ToastAndroid.SHORT);
        }
      })
      .catch(error => {
        console.log('Error create post data', error);
        console.log('Error', error.response);
        console.log('Request', error.request);
        Sentry.captureException(error);
        ToastAndroid.show('Error send post data to server', ToastAndroid.SHORT);
      });
  }

  render() {
    const {
      no_ktp,
      nama_pelanggan,
      tarif,
      daya,
      data_pembangkit,
      images,
    } = this.state;

    return (
      <View style={styles.container}>
        <View style={styles.actionbar}>
          <TouchableOpacity onPress={this._onBackPress}>
            <Icons style={styles.icon} name="arrow-back" size={33} />
          </TouchableOpacity>
          <TextPegawai style={styles.title}>Detail Closing</TextPegawai>
        </View>
        <ScrollView style={{marginBottom: 50}}>
          <Slideshow
            dataSource={images}
            height={400}
            onPress={data => this.showModalImageFunction(true, data)}
          />
          <Card>
            <View>
              <View
                style={{
                  flexDirection: 'row',
                  flexWrap: 'wrap',
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <View style={{flex: 2}}>
                  <Text>No KTP</Text>
                </View>
                <View style={{flexShrink: 1}}>
                  <Text>: </Text>
                </View>
                <View style={{flex: 3}}>
                  <Text>{no_ktp}</Text>
                </View>
              </View>
              <View
                style={{
                  flexDirection: 'row',
                  flexWrap: 'wrap',
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <View style={{flex: 2}}>
                  <Text>Nama Pelanggan</Text>
                </View>
                <View style={{flexShrink: 1}}>
                  <Text>: </Text>
                </View>
                <View style={{flex: 3}}>
                  <Text>{nama_pelanggan}</Text>
                </View>
              </View>
              <View
                style={{
                  flexDirection: 'row',
                  flexWrap: 'wrap',
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <View style={{flex: 2}}>
                  <Text>Tarif</Text>
                </View>
                <View style={{flexShrink: 1}}>
                  <Text>: </Text>
                </View>
                <View style={{flex: 3}}>
                  <Text>{tarif}</Text>
                </View>
              </View>
              <View
                style={{
                  flexDirection: 'row',
                  flexWrap: 'wrap',
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <View style={{flex: 2}}>
                  <Text>Daya</Text>
                </View>
                <View style={{flexShrink: 1}}>
                  <Text>: </Text>
                </View>
                <View style={{flex: 3}}>
                  <Text>{daya}</Text>
                </View>
              </View>
            </View>
          </Card>
          {this.state.isPending == 0 ? (
            <View style={styles.buttonRow}>
              <Button
                buttonStyle={{
                  padding: 10,
                  elevation: 5,
                  borderRadius: 20,
                  alignSelf: 'flex-start',
                }}
                onPress={() => {
                  this.ShowModalPDLFunction(true);
                }}
                backgroundColor="green"
                title=" CLOSING "
              />
            </View>
          ) : null}
        </ScrollView>
        <Modal
          visible={this.state.ModalImageVisible}
          transparent={false}
          onRequestClose={() => {
            this.showModalImageFunction(!this.state.ModalImageVisible);
          }}>
          <ImageViewer
            imageUrls={this.state.imageUrls}
            enableImageZoom={true}
            enableSwipeDown={true}
            onSwipeDown={() => {
              this.showModalImageFunction(!this.state.ModalImageVisible);
            }}
          />
        </Modal>
        <Modal
          transparent={true}
          animationType={'fade'}
          visible={this.state.ModalRayonVisibleStatus}
          onRequestClose={() => {
            this.ShowModalPDLFunction(!this.state.ModalRayonVisibleStatus);
          }}>
          <View
            style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
            <View style={styles.ModalInsideView}>
              <View style={styles.ModalFormInsideView}>
                <Text style={{color: 'black', fontSize: 15}} />
                <View style={styles.separator} />
              </View>
              <View style={styles.separator} />
              <Button
                title="Peremajaan"
                backgroundColor="#0CA3F2"
                icon={{name: 'send', type: 'font-awesome'}}
                onPress={() => {
                  this._onSubmitClosing();
                }}
              />
              <View style={styles.separator} />
              <Button
                backgroundColor="red"
                icon={{name: 'close', type: 'font-awesome'}}
                title="Cancel"
                onPress={() => {
                  this.ShowModalPDLFunction(
                    !this.state.ModalRayonVisibleStatus,
                  );
                }}
              />
            </View>
          </View>
        </Modal>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  actionbar: {
    flexDirection: 'row',
    backgroundColor: colors.blue,
    height: 58,
    alignItems: 'center',
  },
  icon: {
    color: '#fff',
    marginLeft: 12,
    marginRight: 4,
  },
  title: {
    flex: 1,
    marginLeft: 12,
    color: '#fff',
    fontSize: 20,
    fontWeight: 'bold',
  },
  viewTouchable: {
    flex: 1,
    backgroundColor: 'white',
    alignItems: 'center',
    paddingVertical: 20,
    elevation: 5,
  },
  imageTouchable: {
    width: 75,
    height: 75,
    justifyContent: 'center',
    alignItems: 'center',
  },
  separator: {
    height: 0.5,
    marginTop: 8,
    marginBottom: 8,
    backgroundColor: 'grey',
  },
  buttonRow: {
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
    marginTop: 10,
  },
  ModalInsideView: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#fff',
    height: '60%',
    width: '90%',
    borderRadius: 10,
    borderWidth: 3,
    borderColor: '#ccc',
  },
  ModalFormInsideView: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#fff',
    width: '100%',
  },
  pickerForm: {
    width: '60%',
    backgroundColor: colors.grey,
    height: 45,
    padding: 12,
  },
  textAreaForm: {
    width: '60%',
    backgroundColor: colors.grey,
    padding: 12,
    marginBottom: 8,
  },
});

export default ProbingDetail;
