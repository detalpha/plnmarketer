import React, {Component} from 'react';
import {
  Container,
  Header,
  Left,
  Body,
  Button,
  Icon,
  Title,
  Content,
  ToastAndroid,
} from 'native-base';

import {
  FlatList,
  StyleSheet,
  TouchableOpacity,
  Image,
  Dimensions,
  Alert,
} from 'react-native';
import {NavigationActions, StackActions} from 'react-navigation';
import * as Sentry from '@sentry/react-native';

// Asset
import colors from '../../asset/colors';

// API
import {Api} from '../../api/server';

class RewardList extends Component {
  constructor(props) {
    super(props);

    this.state = {
      data: [],
    };

    this._onBackPress = this._onBackPress.bind(this);
    this._refreshData = this._refreshData.bind(this);
    this._takeReward = this._takeReward.bind(this);
  }

  _onBackPress() {
    this.props.navigation.goBack();
  }

  async componentDidMount() {
    this._refreshData();
  }

  async _refreshData() {
    // console.log('Load Data WO')
    // New API
    let api = new Api();
    // Create API
    await api.create();

    let client = api.getClient();
    let api_url = api.getUrl();

    client
      .get('/reward')
      .then(response => {
        console.log(response.data);
        if (response.status == 200) {
          const data = response.data;

          if (data.length > 0) {
            data.map(function(reward) {
              reward.url = api_url + reward.path_image;

              console.log(reward.url);

              return reward;
            });
          }

          this.setState({
            data_reward: data,
            isLoading: false,
          });
        }
      })
      .catch(error => {
        console.log('RefreshWo:error', error);
        Sentry.captureException(error);
      });
  }

  async _takeReward(id_reward) {
    // New API
    let api = new Api();
    // Create API
    await api.create();

    let client = api.getClient();

    // Post data to server
    let data = {
      id_reward: id_reward,
    };

    console.log(data);

    await client
      .post('/reward', data)
      .then(async response => {
        if (response.status == 200) {
          const resetAction = StackActions.reset({
            index: 0,
            actions: [NavigationActions.navigate({routeName: 'Main'})],
          });

          Alert.alert(
            'Redeem Point',
            'Reward berhasil diambil.',
            [
              {
                text: 'OK',
                onPress: () => this.props.navigation.dispatch(resetAction),
              },
            ],
            {cancelable: false},
          );
        } else {
          Alert.alert(
            'Redeem Point',
            'Reward gagal diambil.',
            [
              {
                text: 'OK',
                onPress: () => console.log(response),
              },
            ],
            {cancelable: false},
          );
        }
      })
      .catch(error => {
        console.log('Error create post data', error);
        console.log('Error', error.response);
        console.log('Request', error.request);
        Sentry.captureException(error);
        ToastAndroid.show(error.response.message, ToastAndroid.SHORT);
      });
  }

  render() {
    return (
      <Container>
        <Header>
          <Left>
            <Button transparent onPress={() => this._onBackPress(1)}>
              <Icon name="arrow-back" />
            </Button>
          </Left>
          <Body>
            <Title>Reward</Title>
          </Body>
        </Header>
        <Content>
          <FlatList
            data={this.state.data_reward}
            renderItem={({item}) => (
              <TouchableOpacity
                onPress={() => {
                  Alert.alert(
                    'Redeem Point',
                    'Point anda akan terpotong sebesar ' +
                      item.point +
                      '. Anda yakin ingin menukarkan point anda dengan ' +
                      item.nama_reward +
                      '?',
                    [
                      {
                        text: 'Batal',
                        onPress: () => console.log('Cancel Pressed'),
                        style: 'cancel',
                      },
                      {text: 'Tukar', onPress: () => this._takeReward(item.id)},
                    ],
                    {cancelable: false},
                  );
                }}
                style={{margin: 5}}>
                <Image
                  resizeMode="contain"
                  source={{uri: item.url}}
                  style={{height: 200}}
                />
              </TouchableOpacity>
            )}
          />
        </Content>
      </Container>
    );
  }
}

const win = Dimensions.get('window');

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  actionbar: {
    flexDirection: 'row',
    backgroundColor: colors.blue,
    height: 58,
    alignItems: 'center',
  },
  icon: {
    color: '#fff',
    marginLeft: 12,
    marginRight: 4,
  },
  title: {
    flex: 1,
    marginLeft: 12,
    color: '#fff',
    fontSize: 20,
    fontWeight: 'bold',
  },
  viewTouchable: {
    flex: 1,
    backgroundColor: 'white',
    alignItems: 'center',
    paddingVertical: 20,
    elevation: 5,
  },
  imageTouchable: {
    width: 75,
    height: 75,
    justifyContent: 'center',
    alignItems: 'center',
  },
  separator: {
    height: 0.5,
    marginTop: 8,
    marginBottom: 8,
    backgroundColor: 'grey',
  },
  buttonRow: {
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
    marginTop: 10,
  },
  ModalInsideView: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#fff',
    height: '60%',
    width: '90%',
    borderRadius: 10,
    borderWidth: 3,
    borderColor: '#ccc',
  },
  ModalFormInsideView: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#fff',
    width: '100%',
  },
  pickerForm: {
    width: '60%',
    backgroundColor: colors.grey,
    height: 45,
    padding: 12,
  },
  textAreaForm: {
    width: '60%',
    backgroundColor: colors.grey,
    padding: 12,
    marginBottom: 8,
  },
  list: {
    width: win.width - win.width * 0.06,
  },
});

export default RewardList;
