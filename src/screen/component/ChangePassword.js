import React, {Component} from 'react';
import {
  Container,
  Header,
  Left,
  Body,
  Button,
  Icon,
  Title,
  Content,
  Label,
  Form,
  Input,
  Item,
  Text,
} from 'native-base';

import {StyleSheet, ToastAndroid} from 'react-native';
import * as Sentry from '@sentry/react-native';

// Navigation
import AsyncStorage from '@react-native-community/async-storage';
import {NavigationActions, StackActions} from 'react-navigation';

// API
import {Localhost} from '../../api/localhost';

class ProbingForm extends Component {
  constructor(props) {
    super(props);

    // Bind
    this._onSavePress = this._onSavePress.bind(this);
    this._onBackPress = this._onBackPress.bind(this);

    this.state = {
      form_old_pass: '',
      form_new_pass: '',
      form_renew_pass: '',
    };
  }

  render() {
    return (
      <Container>
        <Header>
          <Left>
            <Button transparent onPress={() => this._onBackPress(1)}>
              <Icon name="arrow-back" />
            </Button>
          </Left>
          <Body>
            <Title>Change Password</Title>
          </Body>
        </Header>
        <Content>
          <Form>
            <Item stackedLabel>
              <Label>Current Password</Label>
              <Input
                onChangeText={text => this.setState({form_old_pass: text})}
              />
            </Item>
            <Item stackedLabel>
              <Label>New Password</Label>
              <Input
                onChangeText={text => this.setState({form_new_pass: text})}
              />
            </Item>
            <Item stackedLabel>
              <Label>Current New Password</Label>
              <Input
                onChangeText={text => this.setState({form_renew_pass: text})}
              />
            </Item>
          </Form>
          <Button block info onPress={this._onSavePress} style={styles.button}>
            <Text>SUBMIT</Text>
          </Button>
        </Content>
      </Container>
    );
  }

  componentDidMount() {}

  async _onSavePress() {
    console.log('mulai save', this.state);
    // New Localhost
    let api = new Localhost();
    // Create API
    await api.create();

    let client = api.getClient();

    // Post data to server
    let data = {
      old_pass: this.state.form_old_pass,
      new_pass: this.state.form_new_pass,
      new_pass_confirmation: this.state.form_renew_pass,
    };

    console.log('Request data', data);

    this.setState({
      btn_loading: true,
      btn_disabled: true,
    });

    await client
      .post('/changepassword', data)
      .then(async response => {
        console.log('Post Laporan WO Response', response.data);

        if (response.status === 200) {
          ToastAndroid.show('Change password success', ToastAndroid.SHORT);

          AsyncStorage.removeItem('user')
            .then(() => {
              console.log('Home:logout = User data removed');
              const resetAction = StackActions.reset({
                index: 0,
                actions: [NavigationActions.navigate({routeName: 'Login'})],
              });
              this.props.navigation.dispatch(resetAction);
            })
            .catch(error => {
              console.log('Home:logout = Error', error);
              Sentry.captureException(error);
              ToastAndroid.show('Logout error', ToastAndroid.SHORT);
            });
        }
      })
      .catch(error => {
        console.log('Error create post data', error);
        console.log('Error', error.response);
        console.log('Request', error.request);
        Sentry.captureException(error);
        ToastAndroid.show(
          'Failed change password. ' + error.response.message,
          ToastAndroid.SHORT,
        );

        this.setState({
          btn_loading: false,
          btn_disabled: false,
        });
      });
  }

  _onBackPress() {
    this.props.navigation.goBack();
  }
}

const styles = StyleSheet.create({
  button: {
    margin: 5,
  },
  imageList: {
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 20,
  },
});

export default ProbingForm;
