import React, {Component} from 'react';
import {
  View,
  StyleSheet,
  TextInput,
  ImageBackground,
  Dimensions,
  ToastAndroid,
} from 'react-native';

import AsyncStorage from '@react-native-community/async-storage';
import * as Sentry from '@sentry/react-native';

import {Container, Content, Button, Text} from 'native-base';

// Navigation
import {NavigationActions, StackActions} from 'react-navigation';

// Colors
import colors from '../asset/colors';

// Window width
const {width} = Dimensions.get('window');

// API
import {Api} from '../api/server';
import {Localhost} from '../api/localhost';

class Login extends Component {
  constructor(props) {
    super(props);

    // Bind Function
    this._onPasswordChange = this._onPasswordChange.bind(this);
    this._onUsernameChange = this._onUsernameChange.bind(this);
    this._onLoginClick = this._onLoginClick.bind(this);

    this.state = {
      username: '',
      password: '',
    };
  }

  render() {
    return (
      <Container>
        <Content>
          <ImageBackground
            source={require('../asset/Capture2.jpeg')}
            style={styles.header}>
            {/* <Image
              style={styles.image}
              source={require('../asset/pln.png')}
              width={width}
              height={170}
            /> */}
          </ImageBackground>
          <View style={styles.formContainer}>
            <TextInput
              style={styles.textForm}
              placeholder="Username"
              underlineColorAndroid="transparent"
              value={this.state.username}
              onChangeText={this._onUsernameChange}
            />
            <TextInput
              style={styles.textForm}
              placeholder="Password"
              underlineColorAndroid="transparent"
              secureTextEntry={true}
              value={this.state.password}
              onChangeText={this._onPasswordChange}
            />

            <View style={styles.separator} />

            <Button block primary onPress={this._onLoginClick}>
              <Text>LOGIN</Text>
            </Button>
          </View>
        </Content>
      </Container>
    );
  }

  _onPasswordChange(text) {
    this.setState({
      password: text,
    });
  }

  _onUsernameChange(text) {
    this.setState({
      username: text,
    });
  }

  async _onLoginClick() {
    console.log('tes');

    // New API
    let api = new Localhost();
    // Create API

    await api.create();

    let client = api.getClient();

    // END new api
    let {navigate} = this.props.navigation;

    // Login logic
    let user = client
      .post('/login', {
        email: this.state.username,
        password: this.state.password,
      })
      .then(async response => {
        console.log('login success', response.data);
        console.log('response', response);
        let data = response.data;

        if (response.status === 200) {
          //OK

          ToastAndroid.show('Login success!', ToastAndroid.SHORT);

          try {
            user = data;
            let token = user.api_token;
            // Remove Token
            delete user.api_token;
            await AsyncStorage.setItem('user', JSON.stringify(user));
            await AsyncStorage.setItem('api_token', token);
          } catch (error) {
            console.log('Error save user data', error);
          }

          // Navigate to Home
          const resetAction = StackActions.reset({
            index: 0,
            actions: [NavigationActions.navigate({routeName: 'Main'})],
          });
          this.props.navigation.dispatch(resetAction);
        }
      })
      .catch(error => {
        console.log('login error', error);
        Sentry.captureException(error);

        if (error.response) {
          // The request was made and the server responded with a status code
          // that falls out of the range of 2xx
          console.log(error.response.data);
          console.log(error.response.status);
          console.log(error.response.headers);
        } else if (error.request) {
          // The request was made but no response was received
          // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
          // http.ClientRequest in node.js
          console.log(error.request);
        } else {
          // Something happened in setting up the request that triggered an Error
          console.log('Error', error.message);
        }
        console.log(error.config);

        if (error.response === 401) {
          ToastAndroid.show(
            'Login failed, ' + error.response.data.message,
            ToastAndroid.SHORT,
          );
          return;
        }

        if (error.response === 422) {
          ToastAndroid.show(
            'Login gagal, mohon periksa kembali',
            ToastAndroid.SHORT,
          );
          return;
        }

        ToastAndroid.show(
          'Login gagal, mohon periksa kembali',
          ToastAndroid.SHORT,
        );
      });

    // // Switch to Main App
    // // navigate('Home')
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  formContainer: {
    flex: 1,
    padding: 20,
    backgroundColor: 'lightgrey',
  },
  textForm: {
    margin: 4,
    backgroundColor: colors.grey,
    height: 50,
    padding: 12,
    fontSize: 20,
    borderColor: colors.border,
    borderWidth: 1,
  },
  loginButton: {
    margin: 4,
    backgroundColor: colors.blue,
    padding: 12,
    justifyContent: 'center',
    alignItems: 'center',
  },
  image: {
    flex: 1,
    height: 320,
    width: 320,
    resizeMode: 'contain',
  },
  header: {
    width: width,
    height: 325,
    padding: 0,
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  separator: {
    height: 0.5,
    marginTop: 8,
    marginBottom: 8,
    backgroundColor: 'grey',
  },
  backgroundImage: {
    flex: 1,
    resizeMode: 'cover', // or 'stretch'
    margin: -75,
  },
});

export default Login;
