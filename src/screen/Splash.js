/* eslint-disable react-native/no-inline-styles */
import React, {Component} from 'react';
import {StyleSheet, ImageBackground, Image, Dimensions} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';

// Navigation
import {NavigationActions, StackActions} from 'react-navigation';
import * as Sentry from '@sentry/react-native';

import {Localhost} from '../api/localhost';

// Window width
const {width, height} = Dimensions.get('window');

class Splash extends Component {
  constructor(props) {
    super(props);

    // Bind
    this._refreshPointData = this._refreshPointData.bind(this);
  }

  render() {
    return (
      <ImageBackground
        source={require('../asset/batik.png')}
        style={{
          width: width,
          height: height,
          alignItems: 'center',
          justifyContent: 'center',
        }}>
        {/* <Image
          source={require('../asset/pln.png')}
          style={{width: width, height: 100}}
        /> */}
      </ImageBackground>
    );
  }

  async componentDidMount() {
    this._refreshPointData();
  }

  async _refreshPointData() {
    console.log('refresh data point');

    // New API
    let api = new Localhost();
    await api.create();
    let user = await AsyncStorage.getItem('user');

    try {
      if (user) {
        let client = api.getClient();
        // Set Loading
        this.setState({isLoading: true});

        client
          .get('/ceklogin')
          .then(response => {
            console.log('refresh data', response);

            if (response.status === 200) {
              // Before render check login state
              console.log('Already Loged in');

              const resetAction = StackActions.reset({
                index: 0,
                actions: [NavigationActions.navigate({routeName: 'Main'})],
              });
              this.props.navigation.dispatch(resetAction);
            }
          })
          .catch(error => {
            console.log('Error create post data', error);
            console.log('Error', error.response);
            console.log('Request', error.request);
            Sentry.captureException(error);
            // Set Loading
            this.setState({isLoading: false});

            if (error.response.status == 401) {
              this.props.navigation.navigate('Login');
            }
          });
      } else {
        // Navigate to login
        this.props.navigation.navigate('Login');
      }
    } catch (error) {
      console.log('Error read user data', error);
      Sentry.captureException(error);
    }
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});
export default Splash;
