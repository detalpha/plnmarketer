import React, {Component} from 'react';
import {
  Container,
  Header,
  Content,
  Footer,
  FooterTab,
  Button,
  Icon,
  Text,
  View,
  Card,
  CardItem,
  Body,
  Left,
  Title,
  Right,
} from 'native-base';

import {
  Dimensions,
  StyleSheet,
  ImageBackground,
  Image,
  ToastAndroid,
  FlatList,
} from 'react-native';

import AsyncStorage from '@react-native-community/async-storage';
import RNFetchBlob from 'rn-fetch-blob';
import Carousel from 'react-native-snap-carousel';

import {ListItem} from 'react-native-elements';
// Asset
import colors from '../asset/colors';

// API
// import server from '../api/server';
import {Localhost} from '../api/localhost';
import * as Sentry from '@sentry/react-native';

// Navigation
import {NavigationActions, StackActions} from 'react-navigation';

// Component
import DrawerItem from './component/DrawerItem';

class Main extends Component {
  constructor(props) {
    super(props);

    // Bind
    this._showProbingOption = this._showProbingOption.bind(this);
    this._showPendingProbing = this._showPendingProbing.bind(this);
    this._showPendingClosing = this._showPendingClosing.bind(this);
    this._showRejectedPoint = this._showRejectedPoint.bind(this);
    this._filterTabData = this._filterTabData.bind(this);
    this._refreshData = this._refreshData.bind(this);
    this._onMenuPress = this._onMenuPress.bind(this);
    this._refreshPointData = this._refreshPointData.bind(this);

    this.state = {
      drawerOpen: false,
      user: {},
      isLoading: false,
      currentTab: '',
      viewstate: 1,
      data: [],
      total_point: 0,
      imagespromo: null,
    };
  }

  render() {
    return (
      <Container>
        <Header>
          <Left />
          <Body>
            <Title>PLN-Marketer</Title>
          </Body>
          <Right />
        </Header>
        <Content padder>
          {this.state.viewstate === 1 ? (
            <View style={{height: '100%'}}>
              <Card style={{height: '100%'}}>
                <CardItem>
                  <Left>
                    <Icon name="md-person" />
                    <Body>
                      <Text>{this.state.name_user}</Text>
                      <Text note>Total Point: {this.state.total_point}</Text>
                    </Body>
                  </Left>
                </CardItem>
                <CardItem>
                  <Left>
                    <Button
                      rounded
                      onPress={() =>
                        this.props.navigation.navigate('RewardList')
                      }
                      style={styles.buttonreward}>
                      <Icon name="md-trophy" />
                      <Text style={{fontSize: 8}}>Rewards</Text>
                    </Button>
                  </Left>
                  <Right>
                    <Button
                      rounded
                      onPress={() =>
                        this.props.navigation.navigate('HistoryList')
                      }
                      style={styles.buttonreward}>
                      <Icon name="md-clock" />
                      <Text style={{fontSize: 8}}>History</Text>
                    </Button>
                  </Right>
                </CardItem>
                {this.state.imagespromo ? (
                  <CardItem>
                    <Carousel
                      data={this.state.imagespromo}
                      height={200}
                      itemWidth={styles.sliderItem.width}
                      sliderWidth={styles.slider.width}
                      onPress={data => this.showModalImageFunction(true, data)}
                      autoplay={true}
                      renderItem={({item}) => (
                        <Image
                          style={{flex: 1}}
                          resizeMode="cover"
                          source={{uri: item.illustration}}
                        />
                      )}
                    />
                  </CardItem>
                ) : null}
              </Card>
            </View>
          ) : null}
          {this.state.viewstate === 2 ? (
            <View style={{height: '100%'}}>
              <Card style={{height: '100%'}}>
                <CardItem>
                  <Body>
                    <Button
                      block
                      info
                      onPress={() => this._showProbingOption('PB')}
                      style={styles.button}>
                      <Text>Pasang Baru</Text>
                    </Button>
                    <Button
                      block
                      info
                      onPress={() => this._showProbingOption('PD')}
                      style={styles.button}>
                      <Text>Perubahan Daya</Text>
                    </Button>
                  </Body>
                </CardItem>
              </Card>
            </View>
          ) : null}
          {this.state.viewstate === 3 ? (
            <FlatList
              data={this.state.data}
              renderItem={({item}) => (
                <ListItem
                  button
                  onPress={() =>
                    this.props.navigation.navigate('ProbingDetail', {
                      id: item.id,
                      isPending: 0,
                    })
                  }
                  roundAvatar
                  title={`Nama Pelanggan: ${item.nama_pelanggan}`}
                  subtitle={item.type}
                  containerStyle={{borderBottomWidth: 1}}
                />
              )}
            />
          ) : null}
          {this.state.viewstate === 4 ? (
            <View>
              <ImageBackground
                style={styles.imageHeader}
                source={require('../asset/Capture2.jpeg')}>
                <Image
                  style={styles.profileHeader}
                  source={require('../asset/default_profil.png')}
                />
                <Text style={styles.textHeader}>
                  {this.state.user.name || 'User'}
                </Text>
              </ImageBackground>
              <View style={styles.menuContainer}>
                <DrawerItem
                  menu="Info Biaya"
                  icon="picture-as-pdf"
                  onPress={() => this._onMenuPress('info_biaya')}
                />
                <DrawerItem
                  menu="Change Password"
                  icon="md-unlock"
                  onPress={() => this._onMenuPress('change_password')}
                />
                <DrawerItem
                  menu="Logout"
                  icon="power-settings-new"
                  onPress={() => this._onMenuPress('logout')}
                />
              </View>
            </View>
          ) : null}
          {this.state.viewstate === 5 ? (
            <View style={{height: '100%'}}>
              <Card style={{height: '100%'}}>
                <CardItem>
                  <Body>
                    <Button
                      block
                      info
                      onPress={() => this._showPendingProbing()}
                      style={styles.button}>
                      <Text>List Pending Probing</Text>
                    </Button>
                    <Button
                      block
                      info
                      onPress={() => this._showPendingClosing()}
                      style={styles.button}>
                      <Text>List Pending CLosing</Text>
                    </Button>
                    <Button
                      block
                      info
                      onPress={() => this._showRejectedPoint()}
                      style={styles.button}>
                      <Text>List Rejected Point</Text>
                    </Button>
                  </Body>
                </CardItem>
              </Card>
            </View>
          ) : null}
        </Content>
        <Footer>
          <FooterTab>
            <Button
              vertical
              onPress={() => {
                this.setState({viewstate: 1});
              }}>
              <Icon name="md-home" />
              <Text style={{fontSize: 8}}>Home</Text>
            </Button>
            <Button
              vertical
              onPress={() => {
                this.setState({viewstate: 2});
              }}>
              <Icon name="md-person-add" />
              <Text style={{fontSize: 8}}>Probing</Text>
            </Button>
            <Button
              vertical
              onPress={() => {
                this.setState({viewstate: 3});
                this._refreshData();
              }}>
              <Icon name="md-list-box" />
              <Text style={{fontSize: 8}}>Closing</Text>
            </Button>
            <Button
              vertical
              onPress={() => {
                this.setState({viewstate: 5});
              }}>
              <Icon name="ios-list-box" />
              <Text style={{fontSize: 8}}>List</Text>
            </Button>
            <Button
              vertical
              onPress={() => {
                this.setState({viewstate: 4});
              }}>
              <Icon name="md-person" />
              <Text style={{fontSize: 8}}>Setting</Text>
            </Button>
          </FooterTab>
        </Footer>
      </Container>
    );
  }

  async componentDidMount() {
    console.log('componentDidMount');
    await this._refreshData();
    await this._refreshPointData();
  }

  _showProbingOption(type) {
    this.props.navigation.navigate('ProbingForm', {type: type});
  }

  _showPendingProbing(type) {
    this.props.navigation.navigate('ProbingList');
  }

  _showPendingClosing(type) {
    this.props.navigation.navigate('PdlList');
  }

  _showRejectedPoint(type) {
    this.props.navigation.navigate('RejectedList');
  }

  async _refreshData() {
    console.log('refresh data');

    let user = JSON.parse(await AsyncStorage.getItem('user'));
    console.log('user data', user);
    this.setState(
      {
        user: user,
        name_user: user.name,
      },
      () => {
        console.log('state', this.state);
      },
    );

    // New API
    let api = new Localhost();
    await api.create();

    let client = api.getClient();
    // Set Loading
    this.setState({isLoading: true});

    // client.get('/laporanCkr/temuan').then((response) => {
    client
      .get('/probing')
      .then(response => {
        console.log('refresh data', response);

        if (response.status === 200) {
          this.setState({
            data: response.data,
            isLoading: false,
          });
        }
      })
      .catch(error => {
        console.log('Error create post data', error);
        console.log('Error', error.response);
        console.log('Request', error.request);
        Sentry.captureException(error);
        // Set Loading
        this.setState({isLoading: false});
      });
  }

  async _refreshPointData() {
    console.log('refresh data point');

    // New API
    let api = new Localhost();
    await api.create();

    let client = api.getClient();
    // Set Loading
    this.setState({isLoading: true});

    // client.get('/laporanCkr/temuan').then((response) => {
    client
      .get('/point')
      .then(response => {
        console.log('refresh data', response);

        if (response.status === 200) {
          this.setState({
            total_point: response.data,
            isLoading: false,
          });
        }
      })
      .catch(error => {
        console.log('Error create post data', error);
        console.log('Error', error.response);
        console.log('Request', error.request);
        Sentry.captureException(error);
        // Set Loading
        this.setState({isLoading: false});
      });
  }

  _filterTabData(jenis) {
    let filteredData;

    if (jenis === '') {
      filteredData = this.state.allData;
    } else if (this.state.allData.length === 0) {
      return;
    } else {
      filteredData = this.state.allData.filter((item, index) => {
        // console.log(jenis, 'jenisnya')
        return item.jenis === jenis;
      });
    }

    // console.log(`FIltered data ${jenis}`,filteredData)

    this.setState({
      filteredData: filteredData,
    });
  }

  async _onMenuPress(menu) {
    switch (menu) {
      case 'info_biaya':
        const api_token = await AsyncStorage.getItem('api_token');

        let api = new Localhost();
        await api.create();
        let api_url = api.getUrl();

        let headers = {
          'Content-Type': 'application/json',
          Accept: 'application/json',
        };

        if (api_token) {
          headers.Authorization = `Bearer ${api_token}`;
        }
        let dirs = RNFetchBlob.fs.dirs;

        console.log(api_url + 'storage/download/info_biaya.pdf');

        RNFetchBlob.config({
          // add this option that makes response data to be stored as a file,
          // this is much more performant.
          fileCache: true,
          appendExt: 'pdf',
          path: dirs.DownloadDir + '/info_biaya.pdf',
        })
          .fetch('GET', api_url + 'storage/download/info_biaya.pdf', headers)
          .then(res => {
            // the temp file path
            console.log('The file saved to ', res.path());

            ToastAndroid.show(
              'Info Bayar sudah di download',
              ToastAndroid.SHORT,
            );
          });

        break;

      case 'logout':
        AsyncStorage.removeItem('user')
          .then(() => {
            console.log('Home:logout = User data removed');
            const resetAction = StackActions.reset({
              index: 0,
              actions: [NavigationActions.navigate({routeName: 'Login'})],
            });
            this.props.navigation.dispatch(resetAction);
          })
          .catch(error => {
            console.log('Home:logout = Error', error);
            Sentry.captureException(error);
            ToastAndroid.show('Logout error', ToastAndroid.SHORT);
          });

        break;

      case 'change_password':
        this.props.navigation.navigate('ChangePassword');
        break;

      default:
        break;
    }
  }
}

const win = Dimensions.get('window');

const drawerwidth = win.width - win.width * 0.06;

const styles = StyleSheet.create({
  buttonreward: {
    width: win.width - win.width * 0.6,
    flexDirection: 'column',
    height: 60,
  },
  button: {
    margin: 5,
  },
  container: {
    flex: 1,
    backgroundColor: 'gainsboro',
  },
  slide: {
    backgroundColor: '#9DD6EB',
    height: 140,
    alignItems: 'center',
    justifyContent: 'center',
  },
  slider: {
    width: win.width - win.width * 0.15,
  },
  sliderItem: {
    width: win.width - win.width * 0.15,
  },
  body: {
    backgroundColor: 'grey',
  },
  imageslide: {
    flex: 1,
    width: win.width,
    height: win.height,
  },
  actionbar: {
    flexDirection: 'row',
    backgroundColor: colors.blue,
    height: 58,
    alignItems: 'center',
    elevation: 1,
  },
  icon: {
    color: '#fff',
    marginLeft: 12,
    marginRight: 4,
  },
  title: {
    marginLeft: 12,
    color: '#fff',
    fontWeight: 'bold',
    fontSize: 20,
    flex: 1,
  },
  namauser: {
    marginRight: 12,
    textAlign: 'right',
    color: '#fff',
    fontWeight: 'bold',
    fontSize: 15,
    flex: 1,
  },
  imageHeader: {
    width: drawerwidth,
    padding: 20,
    height: 200,
    alignItems: 'flex-start',
    justifyContent: 'center',
  },
  textHeader: {
    marginTop: 12,
    fontWeight: 'bold',
    fontSize: 16,
    color: 'black',
  },
  profileHeader: {
    width: 100,
    height: 100,
    borderRadius: 50,
  },
  menuContainer: {
    flex: 1,
    backgroundColor: 'lightgrey',
  },
});

const drawerStyles = {
  drawer: {
    shadowColor: '#000000',
    shadowOpacity: 0.8,
    shadowRadius: 3,
    elevation: 1,
  },
};

export default Main;
