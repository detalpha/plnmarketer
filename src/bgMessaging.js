// @flow
import firebase from 'react-native-firebase';
// Optional flow type
import type { RemoteMessage, Notification } from 'react-native-firebase';

export default async (message: RemoteMessage) => {
    // handle your message


     //set badge android
    // firebase.notifications().getBadge().then((value)=>{
    //     console.log('set badge to: ', value+1)
    //     firebase.notifications().setBadge(value+1)                
    // })


    console.log('message received')
    displayNotification(message.messageId, message.data.title, message.data.body);
    return Promise.resolve();
}

//displaying local notification + badge count
function displayNotification(notifId, title, body){
    const notification = new firebase.notifications.Notification()
            .setNotificationId(notifId)
            .setTitle(title)
            .setBody(body)
            .setSound('default')
            .android.setPriority(firebase.notifications.Android.Priority.Max);
        
    notification.android.setChannelId('channelId').android.setSmallIcon('ic_launcher');

    firebase.notifications().displayNotification(notification)

    //set badge android
    firebase.notifications().getBadge().then((value)=>{
        console.log('set badge to: ', value+1)
        firebase.notifications().setBadge(value+1)                
    })


}