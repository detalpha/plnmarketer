import React, {Component} from 'react';
import NetInfo from '@react-native-community/netinfo';
import Route from './Route';

import * as Sentry from '@sentry/react-native';

Sentry.init({
  dsn: 'https://b4bbe06652194a6bb761840dea7f57fe@sentry.io/1854780',
});

const inititalState = {
  user: {},
};

export default class App extends Component {
  constructor(props) {
    super(props);
    this.CheckConnectivity = this.CheckConnectivity.bind(this);
    this.state = {
      isConnected: true,
    };
  }

  // Check Connection for offline mode
  // -------------------------------------------------------------------------------------------------
  CheckConnectivity = () => {
    NetInfo.isConnected.fetch().then(isConnected => {
      if (isConnected) {
        this.setState({isConnected: true}, () =>
          console.log('CONNECTED SESSION', this.state.isConnected),
        );
      } else {
        this.setState({isConnected: false}, () =>
          console.log('DISCONNECTED SESSION', this.state.isConnected),
        );
      }
    });
  };

  _handleConnectivityChange = isConnected => {
    if (isConnected === true) {
      this.setState({isConnected: true}, () => {
        console.log(
          'change to Connected Session _handleConnectivityChange',
          this.state.isConnected,
        );
      });
    } else {
      this.setState({isConnected: false}, () => {
        console.log(
          'change to Disconnected Session _handleConnectivityChange',
          this.state.isConnected,
        );
      });
    }
  };

  componentWillUnmount() {
    NetInfo.isConnected.removeEventListener(
      'connectionChange',
      this._handleConnectivityChange,
    );
  }

  componentDidMount() {
    this.CheckConnectivity();

    NetInfo.isConnected.addEventListener(
      'connectionChange',
      this._handleConnectivityChange,
    );
  }
  // -------------------------------------------------------------------------------------------------

  render() {
    return (
      <Route
        screenProps={{
          isConnected: this.state.isConnected,
        }}
      />
    );
  }
}
