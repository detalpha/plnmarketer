/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React from 'react';
import {createAppContainer} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';

// Screen Common
import Login from './src/screen/Login';
import Main from './src/screen/Main';
import Splash from './src/screen/Splash';
import Profile from './src/screen/Profile';
import PdlList from './src/screen/component/PdlList';
import ProbingList from './src/screen/component/ProbingList';
import RejectedList from './src/screen/component/RejectedList';
import ProbingForm from './src/screen/component/ProbingForm';
import ProbingOpsi from './src/screen/component/ProbingOpsi';
import ProbingDetail from './src/screen/component/ProbingDetail';
import Home from './src/screen/component/Home';
import PdlHome from './src/screen/component/PdlHome';
import RewardList from './src/screen/component/RewardList';
import HistoryList from './src/screen/component/HistoryList';
import ClosingDetail from './src/screen/component/ClosingDetail';
import ChangePassword from './src/screen/component/ChangePassword';

const AppNavigator = createStackNavigator(
  {
    Splash: {screen: Splash},
    Login: {screen: Login},
    Main: {screen: Main},

    Profile: {screen: Profile},
    PdlList: {screen: PdlList},
    ProbingList: {screen: ProbingList},
    RejectedList: {screen: RejectedList},
    ProbingForm: {screen: ProbingForm},
    ProbingOpsi: {screen: ProbingOpsi},
    ProbingDetail: {screen: ProbingDetail},
    Home: {screen: Home},
    PdlHome: {screen: PdlHome},
    RewardList: {screen: RewardList},
    HistoryList: {screen: HistoryList},
    ClosingDetail: {screen: ClosingDetail},
    ChangePassword: {screen: ChangePassword},
  },
  {
    headerMode: 'none',
  },
);

export default createAppContainer(AppNavigator);
